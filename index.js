const express = require("express"); // Tương tự : import express from "express";

const { drinkClassList, drinkObjectList } = require("./data"); 

// Khởi tạo Express App
const app = express();

const port = 8000;

// Cấu hình để API đọc được body JSON
app.use(express.json());

// Cấu hình để API đọc được body có ký tự tiếng Việt
app.use(express.urlencoded({
    extended: true
}))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (req, res) => {
    res.status(200).json({
        message: "Drinks API"
    })
})

app.get("/drink-class", (req, res) => {
    let code = req.query.code;

    if(code) {
        res.status(200).json({
            drinks: drinkClassList.filter((item) => item.checkDrinkByCode(code))
        })
    } else {
      res.status(200).json({
            drinks: drinkClassList
        })
    }
})

app.get("/drink-class/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    drinkId = parseInt(drinkId); 
    let drinkResponse = null;
    res.status(200).json({
        drinks: drinkClassList.filter((item)=> item.checkDrinkById(drinkId))
    })
})

app.get("/drink-object", (req, res) => {
    let code = req.query.code;

    if(code) {
        // Viết hoa code nhập vào
        code = code.toUpperCase();
        res.status(200).json({
            drinks: drinkObjectList.filter((item) => item.code.includes(code))
        })
    } else {
        res.status(200).json({
            drinks: drinkObjectList
        })
    }
})

app.get("/drink-object/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId); 
    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let drinkResponse = null;
    res.status(200).json({
        drinks: drinkObjectList.filter((item)=> item.id === drinkId)
    })
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
