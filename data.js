// Khai báo class Drink
class Drink {
  constructor(id, code, name, price, createAt, updateAt) {
      this.id = id;
      this.code = code;
      this.name = name;
      this.price = price;
      this.createAt = createAt;
      this.updateAt = updateAt;
  }

  // Hàm kiểm tra đồ uống có chứa ID được truyền vào hay không
  checkDrinkById(id) {
      return this.id === id
  }

  // Hàm kiểm tra đồ uống có code chứa mã được truyền vào hay không
  checkDrinkByCode(code) {
      return this.code.includes(code.toUpperCase());
  }
}

let drinkClassList = [];

// Khởi tạo các class Drink
let traTacClass = new Drink(1, "TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021");
drinkClassList.push(traTacClass);

let cocaClass = new Drink(2, "COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021");
drinkClassList.push(cocaClass);

let pepsiClass = new Drink(3, "PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021");
drinkClassList.push(pepsiClass);

let drinkObjectList = [
  {
      id: 1,
      code: "TRATAC",
      name: "Trà tắc",
      price: 10000,
      createAt: "14/5/2021",
      updateAt: "14/5/2021"
  },
  {
      id: 2,
      code: "COCA",
      name: "Cocacola",
      price: 15000,
      createAt: "14/5/2021",
      updateAt: "14/5/2021"
  },
  {
      id: 3,
      code: "PEPSI",
      name: "Pepsi",
      price: 15000,
      createAt: "14/5/2021",
      updateAt: "14/5/2021"
  }
]

module.exports = {
  drinkClassList,
  drinkObjectList
}